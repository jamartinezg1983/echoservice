package com.kesizo.microservices.client.stream;

import org.springframework.messaging.Message;
import org.springframework.messaging.converter.AbstractMessageConverter;
import org.springframework.util.MimeType;

public class ItemDtoConverter extends AbstractMessageConverter {
	
	public ItemDtoConverter() {
        super(new MimeType("application", "json"));
    }
 
    @Override
    protected boolean supports(Class<?> clazz) {
        return (ItemDto.class == clazz);
    }
 
    @Override
    protected Object convertFromInternal(Message<?> message, 
        Class<?> targetClass, Object conversionHint) {
    	
        Object payload = message.getPayload();
        return payload instanceof ItemDto 
          ? payload
          : new ItemDto();
    }
}
