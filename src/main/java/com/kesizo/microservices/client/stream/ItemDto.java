package com.kesizo.microservices.client.stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

public class ItemDto{
	 
	@JsonValue
	private String message;
 
    public ItemDto(){
        //Para Jackson
    }
 
    @JsonCreator
    public ItemDto(@JsonProperty("message") String message) {
        this.message = message;
    }

}
