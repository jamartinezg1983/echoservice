package com.kesizo.microservices.client.stream;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;

@MessagingGateway
public interface ItemGateway {

	@Gateway(requestChannel = ItemSource.CHANNEL_NAME)
    void generate(@Payload ItemDto itemDto);
}
