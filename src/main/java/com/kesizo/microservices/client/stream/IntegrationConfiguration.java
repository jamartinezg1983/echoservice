package com.kesizo.microservices.client.stream;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;

@Configuration
@EnableBinding(ItemSource.class)
@IntegrationComponentScan
public class IntegrationConfiguration {
	
	/*
	 * This bean is a MessageConverter @StreamMessageConverter add to the MessageConverter list 
	 */
	/*@Bean
    @StreamMessageConverter
    public MessageConverter transferMessageConverter() {
        return new ItemDtoConverter();
    }*/
}
