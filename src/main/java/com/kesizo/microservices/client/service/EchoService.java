package com.kesizo.microservices.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kesizo.microservices.client.stream.ItemDto;
import com.kesizo.microservices.client.stream.ItemGateway;

@Service
public class EchoService {
	
	@Autowired
    private ItemGateway itemGateway;
	
	public String getMessage(String message) {		
		
		final ItemDto itemDto = new ItemDto(message);
        itemGateway.generate(itemDto);
		return message;
	}
}
