package com.kesizo.microservices.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kesizo.microservices.client.service.EchoService;

@RestController
public class EchoServiceController {
	
	private static final String ECHO_SERVICE = "Echo Service";
	
	@Autowired
	private EchoService echoService;
	
	@RequestMapping(value = "/")
	public String echoService() {
		return echoService.getMessage(ECHO_SERVICE);
	}
	
	@GetMapping(value = "/{id}")
	public String echoService(@PathVariable String id) {
		return echoService.getMessage(id);
	}
}
